//
//  ASActivityView.swift
//  Sonic Pool
//
//  Created by Sanchan on 20/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import Foundation
import UIKit

class ERActivityView: UIView
{
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.frame = frame
        self.backgroundColor = UIColor.darkGray
        self.alpha = 0.6
        var activityIndicator = UIActivityIndicatorView()
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
        activityIndicator.frame = CGRect(x: (frame.size.width-50)/2, y: (frame.size.height-50)/2, width: 50, height: 50)
        activityIndicator.startAnimating()
        self.addSubview(activityIndicator)
    }
    
    convenience init () {
        self.init(frame:CGRect.zero)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
}
